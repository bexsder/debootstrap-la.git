# debootstrap-la

#### 介绍
交叉编译构建loongarch64的bootstrap

#### 软件架构
pbuilder + rebootstrap + loongarch patch


#### 安装教程

```
终端一
> sudo apt update
> sudo apt install pbuilder 
> git clone https://gitee.com/bexsder/debootstrap-la.git
> export PATH=/usr/sbin:$PATH
> sudo pbuilder create
> sudo pbuilder login

终端二
> sudo cp -v debootstrap-la/rebootstrap/bootstrap.sh /var/cache/pbuilder/build/xxxxxx/

回到终端一
> ./bootstrap.sh HOST_ARCH=loongarch64 GCC_VER=12
 

```


#### 使用说明

- “回到终端一”，运行脚本后会自动运行交叉编译和bootstrap构建。
- 脚本运行时，会使用reprepro 建立两个本地软件仓库，用于保存临时生成的deb文件。
- 脚本运行时，需要一个用于下载安装amd64平台的构建所需的依赖软件包的外部软件仓库。
- 脚本运行时，也需要一个用于下载源代码包的外部仓库，可以用reprepro在chroot外配置一个代码仓库。
- 源代码需要提前准备好，放到配置的代码仓库里，debian规则文件可使用项目提供的，也可以自己修改。
- 脚本获取软件包和代码，默认都是用sid仓库，所以要提前配置好。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

